const booksAuthor = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const root = document.getElementById("root");
const ul = document.createElement("ul");

booksAuthor.forEach(book => {
    if (book.hasOwnProperty("author") && book.hasOwnProperty ("name") && book.hasOwnProperty ("price")){
        const Li = document.createElement("li");
        Li.textContent = `${book.author}: ${book.name} - ${book.price}`;
        ul.appendChild(Li);
    }else {
        console.error("Не полный обьект кники", book);
    }
})
root.appendChild(ul);




